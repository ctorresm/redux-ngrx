import { createReducer, on } from '@ngrx/store';
import { Todo } from 'src/models/todo';
import {
  add,
  deleteAllTodos,
  deleteTodo,
  editTodo,
  toggleAllStateTodo,
  toggleStateTodo,
} from './todo.actions';

const todo1 = new Todo('Vencer a thanos');
const todo2 = new Todo('Save the world');

export const initialState: Todo[] = [todo1, todo2];

export const todoReducer = createReducer(
  initialState,
  on(add, (state, { texto }) => {
    const todo = new Todo(texto);
    return [todo, ...state];
  }),
  on(toggleStateTodo, (state, { id }) => {
    return state.map((stateToEdit: Todo) => {
      if (stateToEdit.id === id) {
        return {
          ...stateToEdit,
          completado: !stateToEdit.completado,
        };
      } else {
        return stateToEdit;
      }
    });
  }),
  on(editTodo, (state, { id, texto }) => {
    return state.map((stateToEdit) => {
      if (stateToEdit.id === id) {
        return {
          ...stateToEdit,
          texto,
        };
      } else {
        return stateToEdit;
      }
    });
  }),
  on(deleteTodo, (state, { id }) => {
    return state.filter((s) => s.id !== id);
  }),
  on(toggleAllStateTodo, (state, { completado }) => {
    return state.map((s) => {
      return { ...s, completado };
    });
  }),
  on(deleteAllTodos, (state) => {
    return state.filter((s) => s.completado === false);
  })
);
