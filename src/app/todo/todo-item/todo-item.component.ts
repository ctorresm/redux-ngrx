import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import { Todo } from 'src/models/todo';
import { deleteTodo, editTodo, toggleStateTodo } from '../todo.actions';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styles: [],
})
export class TodoItemComponent implements OnInit {
  @Input() todo: Todo;
  @ViewChild('txtInputFisico') txtInputFisico: ElementRef;

  checkField: FormControl;
  textInput: FormControl;

  editando = false;

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.checkField = new FormControl(this.todo.completado);
    this.textInput = new FormControl(this.todo.texto);

    this.checkField.valueChanges.subscribe((valor) => {
      const action = toggleStateTodo({ id: this.todo.id });
      this.store.dispatch(action);
    });
  }

  editar(): void {
    this.editando = true;
    setTimeout(() => {
      this.txtInputFisico.nativeElement.select();
    }, 0);
  }

  terminarEdicion(): void {
    this.editando = false;
    const action = editTodo({ texto: this.textInput.value, id: this.todo.id });
    this.store.dispatch(action);
  }

  deleteTodo(): void {
    const action = deleteTodo({ id: this.todo.id });
    this.store.dispatch(action);
  }
}
