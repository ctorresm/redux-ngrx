import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppState } from 'src/app/app.reducer';
import { FiltrosValidos, setFiltro } from 'src/app/filter/filter.actions';
import { Todo } from 'src/models/todo';
import { deleteAllTodos } from '../todo.actions';

@Component({
  selector: 'app-todo-footer',
  templateUrl: './todo-footer.component.html',
  styles: [],
})
export class TodoFooterComponent implements OnInit {
  filtrosValidos: FiltrosValidos[] = ['todos', 'completados', 'pendientes'];
  filtroActual$: Observable<FiltrosValidos>;
  tareasPendientes = 0;

  constructor(private store: Store<AppState>) {
    this.filtroActual$ = this.store.select('filtro');
    this.store.select('todos').subscribe((todos) => {
      this.contarPendientes(todos);
    });
  }

  ngOnInit(): void {}

  cambiarFiltro(filtroSelected: FiltrosValidos): void {
    const action = setFiltro({ filtro: filtroSelected });
    this.store.dispatch(action);
  }

  contarPendientes(todos: Todo[]): void {
    this.tareasPendientes = todos.filter(
      (todo) => todo.completado === false
    ).length;
  }

  limpiarCompletados(): void {
    const action = deleteAllTodos();
    this.store.dispatch(action);
  }
}
