import { createAction, props } from '@ngrx/store';

export const add = createAction(
  '[TODO] Agregar todo',
  props<{ texto: string }>()
);

export const toggleStateTodo = createAction(
  '[TODO] Cambiar estado todo',
  props<{ id: number }>()
);

export const toggleAllStateTodo = createAction(
  '[TODO] Cambiar estado a todos los todos',
  props<{ completado: boolean }>()
);

export const editTodo = createAction(
  '[TODO] Cambiar nombre al todo',
  props<{ texto: string; id: number }>()
);

export const deleteTodo = createAction(
  '[TODO] Borrar todo',
  props<{ id: number }>()
);

export const deleteAllTodos = createAction('[TODO] Borrar todos los todos');
