import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import { add } from '../todo.actions';

@Component({
  selector: 'app-todo-add',
  templateUrl: './todo-add.component.html',
  styles: [],
})
export class TodoAddComponent implements OnInit {
  constructor(private store: Store<AppState>) {}

  todoInput = new FormControl(null, Validators.required);

  ngOnInit(): void {}

  addTodo(): void {
    if (this.todoInput.invalid) {
      return;
    }
    const action = add({ texto: this.todoInput.value });
    this.store.dispatch(action);
    this.todoInput.reset();
  }
}
