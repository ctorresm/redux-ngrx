import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppState } from 'src/app/app.reducer';
import { FiltrosValidos } from 'src/app/filter/filter.actions';
import { Todo } from 'src/models/todo';

@Component({
  selector: 'app-todos-list',
  templateUrl: './todos-list.component.html',
  styles: [],
})
export class TodosListComponent implements OnInit {
  todos: Todo[];
  filter: FiltrosValidos;

  constructor(private store: Store<AppState>) {
    store.select('todos').subscribe((todos) => (this.todos = todos));
    store.select('filtro').subscribe((f) => (this.filter = f));
  }

  ngOnInit(): void {}
}
