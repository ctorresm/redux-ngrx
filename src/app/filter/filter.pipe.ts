import { Pipe, PipeTransform } from '@angular/core';
import { Todo } from 'src/models/todo';
import { FiltrosValidos } from './filter.actions';

@Pipe({
  name: 'filter',
})
export class FilterPipe implements PipeTransform {
  transform(todos: Todo[], filtro: FiltrosValidos): Todo[] {
    switch (filtro) {
      case 'completados':
        return todos.filter((todo) => todo.completado === true);
      case 'pendientes':
        return todos.filter((todo) => todo.completado === false);
      default:
        return todos;
    }
  }
}
