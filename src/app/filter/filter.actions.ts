import { createAction, props } from '@ngrx/store';

export type FiltrosValidos = 'todos' | 'completados' | 'pendientes';

export const setFiltro = createAction(
  '[FILTRO] Set filtro',
  props<{ filtro: FiltrosValidos }>()
);
