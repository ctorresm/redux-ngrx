import { createReducer, on } from '@ngrx/store';
import { FiltrosValidos, setFiltro } from './filter.actions';

export const initialState: FiltrosValidos = 'todos';

export const filterReducer = createReducer(
  initialState,
  on(setFiltro, (state: FiltrosValidos, { filtro }) => {
    return filtro;
  })
);
