import { ActionReducerMap } from '@ngrx/store';
import { Todo } from 'src/models/todo';
import { FiltrosValidos } from './filter/filter.actions';
import { filterReducer } from './filter/filter.reducer';

import { todoReducer } from './todo/todo.reducer';

export interface AppState {
  todos: Todo[];
  filtro: FiltrosValidos;
}

export const reducers: ActionReducerMap<AppState> = {
  todos: todoReducer,
  filtro: filterReducer,
};
